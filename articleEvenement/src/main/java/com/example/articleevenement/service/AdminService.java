package com.example.articleevenement.service;

import com.example.articleevenement.model.Admin;
import com.example.articleevenement.service.common.CrudService;
import org.springframework.stereotype.Service;

@Service
public class AdminService extends CrudService<Admin> {

    public Admin login(Admin admin) throws Exception {
        Admin adminFound = findWhere(admin, null);
        if (adminFound == null) {
            throw new Exception("Email or password incorrect");
        }
        return adminFound;
    }

}
