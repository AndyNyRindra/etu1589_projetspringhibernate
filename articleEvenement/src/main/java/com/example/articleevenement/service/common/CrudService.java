package com.example.articleevenement.service.common;

import com.example.articleevenement.dao.HibernateDAO;
import com.example.articleevenement.model.common.BaseModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

public class CrudService <T extends BaseModel> {

    @Autowired
    protected HibernateDAO hibernateDAO;

    public void save(T t) throws Exception {
        try {
            hibernateDAO.save(t);
        } catch (Exception e) {
            throw e;
        }
    }

    public void update(T t) throws Exception {
        try {
            hibernateDAO.update(t);
        } catch (Exception e) {
            throw e;
        }
    }

    public void delete(T t) throws Exception {
        try {
            hibernateDAO.delete(t);
        } catch (Exception e) {
            throw e;
        }
    }

    public T findById(Class<T> clazz, Integer id) throws Exception {
        try {

            return hibernateDAO.findById(clazz, id);
        } catch (Exception e) {
            throw e;
        }
    }

    public T findWhere(BaseModel o, Map<String, String> searchCriteria) throws Exception {
        try {
            return hibernateDAO.findWhere(o, searchCriteria);
        } catch (Exception e) {
            throw e;
        }
    }

    public List<T> findAll(BaseModel o, Map<String, String> searchCriteria) throws Exception {
        try {
            return hibernateDAO.findAll(o, searchCriteria);
        } catch (Exception e) {
            throw e;
        }
    }
}
