package com.example.articleevenement.service;

import com.example.articleevenement.model.Post;
import com.example.articleevenement.model.Setting;
import com.example.articleevenement.model.Type;
import com.example.articleevenement.service.common.CrudService;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.articleevenement.util.DateUtil.getDateNow;

@Service
public class PostService extends CrudService<Post> {

    @Override
    public void save(Post post) throws Exception {
        try {
            post.setImage();
            post.setType(hibernateDAO.findById(Type.class, post.getType_id()));
            post.setStatus(0);
            post.setIs_priority(0);
            hibernateDAO.save(post);
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public void update(Post post) throws Exception {
        try {
            post.setImage();
            Post oldPost = findById(Post.class, post.getId());
            oldPost.setStatus(0);
            if (post.getTitle() != null) oldPost.setTitle(post.getTitle());
            if (post.getDescription() != null) oldPost.setDescription(post.getDescription());
            if (post.getDate1() != null ) oldPost.setDate1(post.getDate1());
            if (post.getDate2() != null ) oldPost.setDate2(post.getDate2());
            if (post.getImage() != null) oldPost.setImage(post.getImage());
            if (post.getStatus() != null) oldPost.setStatus(post.getStatus());
            if (post.getLocation() != null) oldPost.setLocation(post.getLocation());
            if (post.getPub_date() != null ) oldPost.setPub_date(post.getPub_date());
            if (post.getIs_priority() != null) oldPost.setIs_priority(post.getIs_priority());
            if (post.getType_id() != null) oldPost.setType(hibernateDAO.findById(Type.class, post.getType_id()));

            hibernateDAO.update(oldPost);
        } catch (Exception e) {
            throw e;
        }
    }

    public void updatePostState(Integer id, Date pubDate) throws Exception {
        try {

            Post postToUpdate = hibernateDAO.findById(Post.class, id);
            postToUpdate.setStatus(1);
            postToUpdate.setPub_date(pubDate);
            hibernateDAO.update(postToUpdate);
        } catch (Exception e) {
            throw e;
        }
    }

    private Map<String, String> getSearchCriteria(String search) {
        Map<String, String> searchCriteria = null;
        if (search != null && search != "") {
            searchCriteria = new HashMap<>();
            searchCriteria.put("title", search);
            searchCriteria.put("description", search);
        }
        return searchCriteria;
    }

    public List<Post> findAll(Post post, Boolean isAuthor, String search) throws Exception {
        Session session = null;
        try {
            session = hibernateDAO.getSessionFactory().openSession();
            Map<String, String> searchCriteria = getSearchCriteria(search);
            if(isAuthor) post.setStatus(1);
            Criteria criteria = hibernateDAO.getFullCriteria(post, searchCriteria, session);
            getListWithResearch(criteria, isAuthor);
            return criteria.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }

    }


    public List<Post> findAllWithResearchAndPagination(Post post, String search, Integer page, Boolean isAuthor) throws Exception {
        Session session = null;
        try {

            if (page == null) page = 1;
            Map<String, String> searchCriteria = getSearchCriteria(search);

            session = hibernateDAO.getSessionFactory().openSession();
            if(isAuthor) post.setStatus(1);
            Criteria criteria = hibernateDAO.getFullCriteria(post, searchCriteria, session);
            getListWithResearch(criteria, isAuthor);
            Setting setting = hibernateDAO.findById(Setting.class, 1);
            Integer limit = Integer.valueOf(setting.getValeur());
            criteria.setFirstResult((page-1)*limit).setMaxResults(limit);
            return criteria.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

    private void getListWithResearch(Criteria criteria, Boolean isAuthor) throws Exception {

        criteria.addOrder(Order.desc("is_priority"));
        if (isAuthor) {
            criteria.add(Restrictions.le("pub_date", getDateNow()));
        }
    }

    public Integer getPagesCount(Post post, String search, Boolean isAuthor) throws Exception {
        try {
            Setting setting = hibernateDAO.findById(Setting.class, 1);
            Integer limit = Integer.valueOf(setting.getValeur());
            return (int)Math.ceil((double) findAll(post, isAuthor, search).size() / limit);
        } catch (Exception e) {
            throw e;
        }
    }
}
