package com.example.articleevenement.service;

import com.example.articleevenement.model.Setting;
import com.example.articleevenement.service.common.CrudService;
import org.springframework.stereotype.Service;

@Service
public class SettingService extends CrudService<Setting> {

    public void updateRowsNumber(String newValue) throws Exception {
        Setting setting = null;
        try {
            setting = findById(Setting.class, 1);
            setting.setValeur(newValue);
            update(setting);
        } catch (Exception e) {
            throw e;
        }

    }
}
