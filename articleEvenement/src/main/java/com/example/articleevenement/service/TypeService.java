package com.example.articleevenement.service;

import com.example.articleevenement.model.Type;
import com.example.articleevenement.service.common.CrudService;
import org.springframework.stereotype.Service;

@Service
public class TypeService extends CrudService<Type> {
}
