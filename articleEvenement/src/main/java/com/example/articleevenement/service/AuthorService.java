package com.example.articleevenement.service;

import com.example.articleevenement.model.Admin;
import com.example.articleevenement.model.Author;
import com.example.articleevenement.service.common.CrudService;
import org.springframework.stereotype.Service;

@Service
public class AuthorService extends CrudService<Author> {

    public Author login(Author author) throws Exception {
        Author authorFound = findWhere(author, null);
        if (authorFound == null) {
            throw new Exception("Email or password incorrect");
        }
        return authorFound;
    }
}
