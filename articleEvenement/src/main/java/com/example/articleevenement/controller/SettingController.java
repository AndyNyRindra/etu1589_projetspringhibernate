package com.example.articleevenement.controller;

import com.example.articleevenement.model.Post;
import com.example.articleevenement.model.Setting;
import com.example.articleevenement.model.Type;
import com.example.articleevenement.service.SettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/setting")
public class SettingController {

    @Autowired
    private SettingService service;

    @Autowired
    private HttpSession session;

    @GetMapping("/update/rowsnumber")
    public ModelAndView loadPostForm(@RequestParam(required = false) String error) throws Exception {
        if (session.getAttribute("admin") == null) {
            return new ModelAndView("redirect:/admin/login");
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("command", service.findById(Setting.class, 1));
        modelAndView.addObject("error", error);
        modelAndView.setViewName("rowsNumberForm");
        return modelAndView;
    }

    @PostMapping("/update/rowsnumber")
    public ModelAndView updateRowsNumber(@RequestParam Integer valeur) throws Exception {
        if (session.getAttribute("admin") == null) {
            return new ModelAndView("redirect:/admin/login");
        }
        ModelAndView modelAndView = new ModelAndView();
        try {
            service.updateRowsNumber(String.valueOf(valeur));
            modelAndView.setViewName("redirect:/post/admin");
        } catch (Exception e) {
            e.printStackTrace();
            modelAndView.setViewName("redirect:/setting/update/rowsnumber?error=" + e.getMessage());

        }
        return modelAndView;
    }
}
