package com.example.articleevenement.controller;

import com.example.articleevenement.model.Author;
import com.example.articleevenement.model.Post;
import com.example.articleevenement.model.Type;
import com.example.articleevenement.service.PostService;
import com.example.articleevenement.service.TypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Date;

import static com.example.articleevenement.util.ControllerUtil.getMessage;

@Controller
@RequestMapping("/post")
public class PostController {

    @Autowired
    private PostService postService;

    @Autowired
    private TypeService typeService;

    @Autowired
    private HttpSession session;

    @GetMapping("/new")
    public ModelAndView loadPostForm(@RequestParam(required = false) String error) throws Exception {
        if (session.getAttribute("author") == null) {
            return new ModelAndView("redirect:/author/login");
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("command", new Post());
        modelAndView.addObject("types", typeService.findAll(new Type(), null));
        modelAndView.addObject("error", error);
        modelAndView.addObject("side", "author");
        modelAndView.addObject("event", "save");
        modelAndView.addObject("action", "post");
        modelAndView.setViewName("postForm");
        return modelAndView;
    }

    @GetMapping("{id}/update")
    public ModelAndView loadUpdateForm(@PathVariable Integer id, @RequestParam(required = false) String error) throws Exception {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("command", postService.findById(Post.class, id));
        modelAndView.addObject("error", error);
        if (session.getAttribute("admin") != null) {
            modelAndView.addObject("side", "admin");
        } else {
            modelAndView.addObject("side", "author");
        }

        modelAndView.addObject("types", typeService.findAll(new Type(), null));
        modelAndView.addObject("event", "update");
        modelAndView.addObject("action", "post/" + id );
        modelAndView.setViewName("postForm");
        return modelAndView;
    }

    @PostMapping
    public ModelAndView savePost(@ModelAttribute Post post, BindingResult bindingResult) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            String message = getMessage(bindingResult);
            modelAndView.setViewName("redirect:/post/new?error=" + message);
            return modelAndView;
        }
        try {
            Author author = (Author) session.getAttribute("author");
            post.setAuthor(author);
            postService.save(post);
            modelAndView.setViewName("redirect:/post/author");
        } catch (Exception e) {
            e.printStackTrace();
            modelAndView.setViewName("redirect:/post/new?error=" + e.getMessage());
        }
        return modelAndView;
    }

    @PostMapping("{id}")
    public ModelAndView updatePost(@PathVariable Integer id, @ModelAttribute Post post, BindingResult bindingResult) throws Exception {
        ModelAndView modelAndView = new ModelAndView();

        if (bindingResult.hasErrors()) {

        }

        try {
            post.setId(id);
            postService.update(post);
            if (session.getAttribute("author") != null) {
                modelAndView.setViewName("redirect:/post/author");
            } else if (session.getAttribute("admin") != null) {
                modelAndView.setViewName("redirect:/post/admin");
            }
        } catch (Exception e) {
            modelAndView.setViewName("redirect:/post/" + id + "/update?error=" + e.getMessage());

        }
        return modelAndView;
    }

    @GetMapping("/admin")
    public ModelAndView loadPostsForBack(@RequestParam(required = false) Integer page, @RequestParam(required = false) String search, @RequestParam(required = false) Integer status, @RequestParam(required = false) String error) throws Exception {
        if (session.getAttribute("admin") == null) {
            return new ModelAndView("redirect:/admin/login");
        }
        ModelAndView modelAndView = new ModelAndView();
        Post post = new Post();
        if (status != null && status != 2) {
            post.setStatus(status);
        }
        getListModelAndView(page, search, modelAndView, post, error, false);
        if (status != null) {
            modelAndView.addObject("status", status);
        }
        modelAndView.addObject("side", "admin");
        modelAndView.setViewName("postList");
        return modelAndView;
    }


    @GetMapping("/front")
    public ModelAndView loadPostsForFront(@RequestParam(required = false) Integer page, @RequestParam(required = false) String search, @RequestParam(required = false) String error) throws Exception {
//        if (session.getAttribute("author") == null) {
//            return new ModelAndView("redirect:/author/login");
//        }
        ModelAndView modelAndView = new ModelAndView();
        Post post = new Post();
        post.setStatus(1);
        getListModelAndView(page, search, modelAndView, post, error, true);
        modelAndView.addObject("side", "front");
        modelAndView.setViewName("postList");
        return modelAndView;
    }

    @GetMapping("/author")
    public ModelAndView loadAuthorsPosts(@RequestParam(required = false) Integer page, @RequestParam(required = false) String search, @RequestParam(required = false) Integer status, @RequestParam(required = false) String error) throws Exception {
        if (session.getAttribute("author") == null) {
            return new ModelAndView("redirect:/author/login");
        }
        ModelAndView modelAndView = new ModelAndView();
        Post post = new Post();
        if (status != null && status != 2) {
            post.setStatus(status);
        }
        post.setAuthor((Author) session.getAttribute("author"));
        getListModelAndView(page, search, modelAndView, post, error, false);
        if (status != null) {
            modelAndView.addObject("status", status);
        }
        modelAndView.addObject("side", "author");
        modelAndView.setViewName("postList");
        return modelAndView;
    }

    private void getListModelAndView(Integer page, String search, ModelAndView modelAndView, Post post, String error, Boolean isAuthor) throws Exception {
        modelAndView.addObject("posts", postService.findAllWithResearchAndPagination(post, search, page, isAuthor));
        if (search != null) {
            modelAndView.addObject("search", search);
        }
        modelAndView.addObject("error", error);
        modelAndView.addObject("pageCount", postService.getPagesCount(post, search, isAuthor));
        if (page != null) {
            modelAndView.addObject("page", page);
        } else {
            modelAndView.addObject("page", 1);
        }
    }

    @PostMapping("{id}/validate")
    public ModelAndView validate(@PathVariable Integer id, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm") Date pubDate) throws Exception {
        System.out.println("id is " + id);
        ModelAndView modelAndView = new ModelAndView();
        try {
            postService.updatePostState(id, pubDate);
            modelAndView.setViewName("redirect:/post/admin");
        } catch (Exception e) {
            modelAndView.setViewName("redirect:/post/admin?error=" + e.getMessage());
        }

        return modelAndView;
    }

}
