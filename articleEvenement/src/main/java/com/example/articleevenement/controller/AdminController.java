package com.example.articleevenement.controller;

import com.example.articleevenement.model.Admin;
import com.example.articleevenement.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import static com.example.articleevenement.util.ControllerUtil.getMessage;

@Controller
@RequestMapping ("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private HttpSession session;

    @GetMapping("/login")
    public ModelAndView loadLoginForm(@RequestParam(required = false) String error) {
        session.removeAttribute("admin");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("command", new Admin());
        modelAndView.addObject("error", error);
        modelAndView.addObject("side", "admin");
        modelAndView.setViewName("loginForm");
        return modelAndView;
    }

    @PostMapping("/login")
    public ModelAndView login(@ModelAttribute Admin admin, BindingResult bindingResult) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            String message = getMessage(bindingResult);
            modelAndView.setViewName("redirect:/admin/login?error=" + message);
            return modelAndView;
        }
        try {
            admin = adminService.login(admin);
            session.setAttribute("admin", admin);
            return new ModelAndView("redirect:/post/admin");
        } catch (Exception e) {
            modelAndView.setViewName("redirect:/admin/login?error=" + e.getMessage());
            return modelAndView;
        }
    }
}
