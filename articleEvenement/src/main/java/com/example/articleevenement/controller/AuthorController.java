package com.example.articleevenement.controller;

import com.example.articleevenement.model.Admin;
import com.example.articleevenement.model.Author;
import com.example.articleevenement.service.AdminService;
import com.example.articleevenement.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

import static com.example.articleevenement.util.ControllerUtil.getMessage;

@Controller
@RequestMapping("/author")
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @Autowired
    private HttpSession session;

    @GetMapping("/login")
    public ModelAndView loadLoginForm(@RequestParam(required = false) String error) {
        session.removeAttribute("author");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("command", new Author());
        modelAndView.addObject("error", error);
        modelAndView.addObject("side", "author");
        modelAndView.setViewName("loginForm");
        return modelAndView;
    }

    @PostMapping("/login")
    public ModelAndView login(@ModelAttribute Author author, BindingResult bindingResult) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        if (bindingResult.hasErrors()) {
            String message = getMessage(bindingResult);

            modelAndView.setViewName("redirect:/author/login?error="+ message);
            return modelAndView;
        }
        try {
            author = authorService.login(author);
            session.setAttribute("author", author);
            return new ModelAndView("redirect:/post/author");
        } catch (Exception e) {
            modelAndView.setViewName("redirect:/author/login?error=" + e.getMessage());
            return modelAndView;
        }
    }
}
