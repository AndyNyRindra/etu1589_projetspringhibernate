package com.example.articleevenement.model;

import com.example.articleevenement.model.common.HasName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
public class Type extends HasName {

}
