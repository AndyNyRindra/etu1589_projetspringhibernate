package com.example.articleevenement.model.common;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Getter
@Setter
public class HasName extends BaseModel {
    private String name;
}
