package com.example.articleevenement.model;

import com.example.articleevenement.model.common.HasName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public class Person extends HasName {

    private String email;
    private String password;
}
