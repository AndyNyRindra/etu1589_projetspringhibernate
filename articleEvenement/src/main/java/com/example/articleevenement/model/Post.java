package com.example.articleevenement.model;

import com.example.articleevenement.model.common.BaseModel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.Date;

import static com.example.articleevenement.util.DateUtil.formatDate;
import static com.example.articleevenement.util.DateUtil.getDateNow;
import static com.example.articleevenement.util.ImageUtil.getBase64Image;

@Entity
@Getter
@Setter
public class Post extends BaseModel {
    private String title;
    private String description;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date date1;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date date2;

    @ManyToOne
    private Type type;

    private String location;

    private Integer status;

    @ManyToOne
    private Author author;

    @Column(columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP", insertable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date creation_date;

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private Date pub_date;

    private Integer is_priority;

    @Transient
    private Integer type_id;

    private String image;

    @Transient
    private MultipartFile imageFile;

    public void setImage() throws Exception {
        if (getImageFile() != null && !getImageFile().isEmpty()) {
            try {
                image = getBase64Image(imageFile);
            } catch (Exception e) {
                throw e;
            }
        }
    }

    public void setDate1(Date date) throws Exception {
        if (getDate2() != null) {
            if (date.after(getDate2())) {
                this.date1 = date;
                throw new Exception("La date 1 doit etre anterieur a la date 2");
            }
        }
        this.date1 = date;
    }

    public void setDate2(Date date) throws Exception {
        if (getDate1() != null) {
            if (date.before(getDate1())) {
                this.date2 = date;
                throw new Exception("La date 2 doit etre posterieur a la date 1");
            }
        }
        this.date2 = date;
    }

    public void setPub_date(Date date) throws Exception {
        if (getPub_date() != null && date.compareTo(getPub_date()) == 0) {
            this.pub_date = date;
        }
        else if (date != null && date.before(getDateNow())) {
            this.pub_date = date;
            throw new Exception("La date de publication doit etre posterieur a aujourd'hui");
        }
        this.pub_date = date;

    }

    public String getDate1ToString() {
        return formatDate(getDate1());
    }

    public String getDate2ToString() {
        return formatDate(getDate2());
    }

    public String getDateCreationToString() {
        return formatDate(getCreation_date());
    }

    public String getDatePubToString() {
        return formatDate(getPub_date());
    }

    public String getTitleToUpperCase() {
        return getTitle().toUpperCase();
    }

    public Boolean isArticle() {
        if (getType() != null && getType().getId() == 1) {
            return true;
        }
        return false;
    }
}
