package com.example.articleevenement.dao;

import com.example.articleevenement.model.common.BaseModel;

import java.util.List;
import java.util.Map;

public interface InterfaceDAO {

    public void save(BaseModel o) throws Exception;
    public <T extends BaseModel> List<T> findAll(BaseModel o, Map<String, String> searchCriteria);
    public <T extends BaseModel> T findById(Class<T> clazz, Integer id) throws Exception;
    public <T extends BaseModel> T findWhere(BaseModel o, Map<String, String> searchCriteria) throws Exception;
    public void update(BaseModel o) throws Exception;
    public void delete(BaseModel o) throws Exception;

    public <T extends BaseModel> List<T> findAllWithPagination(BaseModel o, Integer offset, Integer limit, Map<String, String> searchCriteria);
}
