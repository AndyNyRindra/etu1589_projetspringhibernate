package com.example.articleevenement.dao;

import com.example.articleevenement.model.common.BaseModel;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.internal.SessionImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.example.articleevenement.util.DateUtil.getDateNow;

public class HibernateDAO implements InterfaceDAO {

    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(BaseModel o) throws Exception {
        Session session = null;
        Transaction tx = null;
        try {
            session = this.sessionFactory.openSession();
            tx = session.beginTransaction();
            session.save(o);
            tx.commit();
            System.out.println("Saved");
        } catch (Exception e) {
            tx.rollback();
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

    @Override
    public void update(BaseModel o) throws Exception {
        Session session = null;
        Transaction tx = null;
        try {
            session = getSessionFactory().openSession();
            tx = session.beginTransaction();
            update(o, session);
            tx.commit();
            System.out.println("Updated");
        } catch (Exception e) {
            tx.rollback();
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

    public void update(BaseModel o, Session session) throws Exception {
        session.update(o);
    }

    @Override
    public void delete(BaseModel o) throws Exception {
        Session session = null;
        Transaction tx = null;
        try {
            session = getSessionFactory().openSession();
            tx = session.beginTransaction();
            delete(o, session);
            tx.commit();
            System.out.println("Deleted");
        } catch (Exception e) {
            tx.rollback();
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

    public void delete(BaseModel o, Session session) throws Exception {
        session.delete(o);
    }

    private Criteria getCriteriaWithAndCondition(BaseModel o, Session session) {
        try {
            Criteria criteria = session.createCriteria(o.getClass());
            Example example = Example.create(o);
            criteria.add(example);
            return criteria;
        } catch (Exception e) {
            throw e;
        }
    }

    private <T extends BaseModel> Criteria getCriteriaWithOrCondition(Class<T> clazz, Map<String, String> searchCriteria, Session session) {
        try {
            Criteria criteria = session.createCriteria(clazz);

            if (searchCriteria != null) {
                List<Criterion> criterions = new ArrayList<>();
                for (Map.Entry<String, String> entry : searchCriteria.entrySet()) {
                    criterions.add(Restrictions.ilike(entry.getKey(), entry.getValue(), MatchMode.ANYWHERE));
                }

                criteria.add(Restrictions.or(criterions.toArray(new Criterion[criterions.size()])));

            }
            return criteria;
        } catch (Exception e) {
            throw e;
        }
    }

    private Criteria getCriteria(BaseModel o, Map<String, String> searchCriteria, Session session) {
        try {
            if (searchCriteria != null && searchCriteria.size() > 0) {
                return getCriteriaWithOrCondition(o.getClass(), searchCriteria, session);
            } else {
                return getCriteriaWithAndCondition(o, session);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public Criteria getFullCriteria(BaseModel o, Map<String, String> searchCriteria, Session session) {
        Criteria criteria = getCriteriaWithOrCondition(o.getClass(), searchCriteria, session);
        Example example = Example.create(o);
        criteria.add(example);
        return criteria;
    }


    @Override
    public <T extends BaseModel> List<T> findAll(BaseModel o, Map<String, String> searchCriteria) {
        Session session = null;
        try {
            session = getSessionFactory().openSession();
            Criteria criteria = getFullCriteria(o, searchCriteria, session);

            return criteria.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

    @Override
    public <T extends BaseModel> T findById(Class<T> clazz, Integer id) throws Exception {
        Session session = null;
        T result = null;
        try {
            session = getSessionFactory().openSession();
            return session.get(clazz, id);
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

    @Override
    public <T extends BaseModel> T findWhere(BaseModel o, Map<String, String> searchCriteria) throws Exception {
        try {
             if (findAll(o, searchCriteria) != null && findAll(o, searchCriteria).size() > 0) {
                 return (T) findAll(o, searchCriteria).get(0);
             }
             return null;
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public <T extends BaseModel> List<T> findAllWithPagination(BaseModel o, Integer offset, Integer limit, Map<String, String> searchCriteria) {
        Session session = null;
        try {
            session = getSessionFactory().openSession();
            Criteria criteria = getFullCriteria(o, searchCriteria, session);

            criteria.setFirstResult(offset).setMaxResults(limit);
            return criteria.list();
        } catch (Exception e) {
            throw e;
        } finally {
            if (session != null) session.close();
        }
    }

}
