package com.example.articleevenement.util;

import org.springframework.web.multipart.MultipartFile;

import java.util.Base64;

public class ImageUtil {

    public static String getBase64Image(MultipartFile imageFile) throws Exception {
        byte[] fileBytes = imageFile.getBytes();
        return "data:" + imageFile.getContentType() + ";base64," + Base64.getEncoder().encodeToString(fileBytes);
    }
}
