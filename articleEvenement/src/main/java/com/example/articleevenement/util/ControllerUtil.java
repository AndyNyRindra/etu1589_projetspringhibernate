package com.example.articleevenement.util;

import org.springframework.validation.BindingResult;

public class ControllerUtil {

    public static String getMessage(BindingResult bindingResult) {
        String message = "";
        if (bindingResult.getAllErrors() != null && bindingResult.getAllErrors().size() > 0) {
            if (bindingResult.getAllErrors().get(0).getDefaultMessage().split(":").length > 1) {
                message = bindingResult.getAllErrors().get(0).getDefaultMessage().split(":")[1];
            }
        }
        if (message == "") {
            message = "Invalid data";
        }
        return message;
    }
}
