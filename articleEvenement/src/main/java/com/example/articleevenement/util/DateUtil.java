package com.example.articleevenement.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

public class DateUtil {

    public static Date getDateNow() {
        return Timestamp.valueOf(LocalDateTime.now());
    }


    public static String formatDate (Date date) {
        SimpleDateFormat DateFor = new SimpleDateFormat("dd/MM/yyyy 'a' HH:mm");
        return DateFor.format(date);
    }

}
