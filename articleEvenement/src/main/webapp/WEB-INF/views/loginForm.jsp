<%--
  Created by IntelliJ IDEA.
  User: andyn
  Date: 29/01/2023
  Time: 12:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="header_${side}.jsp" />
<div class="container-fluid bg-light py-5">
<div class="row">
    <div class="col-md-6 m-auto text-center">
        <h1 class="h1">Login</h1>
    </div>

</div>
</div>
<div class="container py-5">
    <div class="row py-5">
        <form:form method="post" action="${pageContext.request.contextPath}/${side}/login" class="col-md-9 m-auto">
            <div class="row">
                <div class="form-group col-md-6 mb-3">
                    <label for="email">Email</label>
                    <form:input path="email" type="email" class="form-control mt-1" id="name" placeholder="Name"/>
                </div>
                <div class="form-group col-md-6 mb-3">
                    <label for="password">Password</label>
                    <form:input path="password" type="password" class="form-control mt-1" id="password" placeholder="Password"/>
                </div>
            </div>

            <div class="row">
                <div class="col text-end mt-2">
                    <button type="submit" class="btn btn-success btn-lg px-3">Login</button>
                </div>
            </div>
        </form:form>
        <p>${error}</p>
    </div>
</div>


<jsp:include page="footer_${side}.jsp" />
</html>
