<%--
  Created by IntelliJ IDEA.
  User: andyn
  Date: 25/01/2023
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<jsp:include page="header_admin.jsp" />



<div class="container-fluid bg-light py-5">
    <div class="row">
        <div class="col-md-6 m-auto text-center">
            <h1 class="h1">Update Rows Number Per Page</h1>
        </div>

    </div>
</div>


<div class="container py-5">
    <div class="row py-5">
        <form:form method="post" action="${pageContext.request.contextPath}/setting/update/rowsnumber" class="col-md-9 m-auto">

            <div class="mb-3">
                <label for="title">Value</label>
                <form:input path="valeur" type="number" class="form-control mt-1" id="title" placeholder="Value" value="${command.valeur}"/>
            </div>


            <div class="row">
                <div class="col text-end mt-2">
                    <button type="submit" class="btn btn-success btn-lg px-3">Update</button>
                </div>
            </div>
        </form:form>
        <p>${error}</p>
    </div>
</div>


<jsp:include page="footer_admin.jsp" />
</html>
