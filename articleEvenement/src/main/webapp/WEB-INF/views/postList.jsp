<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header_${side}.jsp" />

<div class="container-fluid bg-light py-5">
<div class="row">
    <div class="col-md-6 m-auto text-center">
        <h1 class="h1">Posts</h1>
    </div>

</div>
</div>
<c:if test='${error != null && error != ""}'>
    <script>alert("${error}")</script>
</c:if>
<div class="row">
<c:forEach var="post" items="${posts}">
    <div class="col-md-4 ${post.type.name}">
        <div class="card mb-4 product-wap rounded-0">
            <div class="card rounded-0">
                <img class="card-img rounded-0 img-fluid" src="${post.image}">

                <c:if test='${side != "front"}'>

                <div class="card-img-overlay rounded-0 product-overlay d-flex align-items-center justify-content-center">
                    <ul class="list-unstyled">
                        <li><a class="btn btn-success text-white" href="${pageContext.request.contextPath}/post/${post.id}/update">Update</a></li>
                    </ul>
                </div>
                </c:if>
            </div>
            <div class="card-body">
                <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                    <li><span class="far">by ${post.author.name}</span></li>
                </ul>
                <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                    <li><span class="far">Creation: ${post.dateCreationToString}</span></li>
                </ul>
                <c:if test="${post.pub_date != null}">
                    <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                        <li><span class="far">Publication: ${post.datePubToString}</span></li>
                    </ul>
                </c:if>
                <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                    <li><a href=""><span class="far fa-heading ${post.type.name}"> ${post.titleToUpperCase}</span></a></li>
                    <li>${post.type.name}</li>
                </ul>
                <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                    <li><span class="far fa-calendar"> ${post.date1ToString}</span></li>
                    <c:if test="${post.date2 != null && post.type.id == 2}">
                        <li><span class="far fa-calendar"> ${post.date2ToString}</span></li>
                    </c:if>
                </ul>
                <c:if test="${post.location != null}">
                <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">
                    <li><span class="far fa-map-pin">${post.location}</span></li>
                </ul>
                </c:if>
<%--                <c:if test='${post.status == 0 && side == "admin" }'>--%>
<%--                    <ul class="w-100 list-unstyled d-flex justify-content-between mb-0">--%>
<%--                        <form action="${pageContext.request.contextPath}/post/${post.id}/validate" method="post">--%>
<%--                            <input type = "datetime-local" name="pubDate" class="form-control" required>--%>
<%--                            <input type="submit" value="Publier" class="btn btn-success">--%>
<%--                        </form>--%>
<%--&lt;%&ndash;                        <a href="${pageContext.request.contextPath}/post/${post.id}/validate" class="btn btn-success">Publier</a>&ndash;%&gt;--%>
<%--                    </ul>--%>
<%--                </c:if>--%>
            </div>
        </div>
    </div>
</c:forEach>
</div>
<div div="row">
    <ul class="pagination pagination-lg justify-content-center">
        <% String search = "";
            if (request.getAttribute("search") != null) { search = "&search=" + request.getAttribute("search"); } %>
        <% String status = "";
            if (request.getAttribute("status") != null) { status = "&status=" + request.getAttribute("status"); } %>
        <% if(!request.getAttribute("page").equals(1)) { %>
        <li class="page-item">
            <a class="page-link rounded-0 shadow-sm border-top-0 border-left-0 text-dark" href='<%=(request.getContextPath()+"/post/" + request.getAttribute("side")+ "?page="+(  (int)request.getAttribute("page") - 1  ) + search + status)%>'><%="<<"%></a>
        </li>
        <% } %>
        <% for(int i = 1 ; i <= (int)request.getAttribute("pageCount"); i++) { %>

        <% if(request.getAttribute("page").equals(i)) { %>
        <li class="page-item disabled">
            <a class="page-link active rounded-0 shadow-sm border-top-0 border-left-0 text-dark" href='<%=(request.getContextPath()+"/post/" + request.getAttribute("side") +"?page="+i+search + status)%>'><%=i%></a>
        </li>
        <% } else if(!request.getAttribute("page").equals(i)) { %>
        <li class="page-item">
            <a class="page-link rounded-0 shadow-sm border-top-0 border-left-0 text-dark" href='<%=(request.getContextPath()+"/post/" + request.getAttribute("side") +"?page="+i+search + status)%>'><%=i%></a>
        </li>
        <% }} %>
        <% if(!request.getAttribute("page").equals(request.getAttribute("pageCount")) && !request.getAttribute("pageCount").equals(0)) { %>
        <li class="page-item">
            <a class="page-link rounded-0 shadow-sm border-top-0 border-left-0 text-dark" href='<%=(request.getContextPath()+"/post/" + request.getAttribute("side") +"?page="+(  (int)request.getAttribute("page") + 1  ) + search + status)%>'><%=">>"%></a>
        </li>
        <% } %>
    </ul>
</div>

<jsp:include page="footer_${side}.jsp" />
</html>