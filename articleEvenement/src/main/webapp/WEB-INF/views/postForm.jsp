<%@ page import="com.example.articleevenement.model.Post" %>
<%@ page import="com.example.articleevenement.model.Type" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: andyn
  Date: 25/01/2023
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header_${side}.jsp" />

<script>
  function activateDate2() {
    var type = document.getElementById("type");
    if (type.value == 2) {
      document.getElementById("date2").value = "${command.date2}";
      document.getElementById("date2").disabled = false;

    } else if(type.value == 1) {
      document.getElementById("date2").value = "";
      document.getElementById("date2").disabled = true;
    }
  }
</script>

<div class="container-fluid bg-light py-5">
  <div class="row">
    <div class="col-md-6 m-auto text-center">
<c:if test='${event == "save" }'>
      <h1 class="h1">Add a Post</h1>
</c:if>
      <c:if test='${event == "update" }'>
        <h1 class="h1">Update a Post</h1>
      </c:if>
    </div>

  </div>
</div>


<div class="container py-5">
  <div class="row py-5">

  <form:form method="post" action="${pageContext.request.contextPath}/${action}" enctype="multipart/form-data" class="col-md-9 m-auto">

    <div class="mb-3">
      <label for="type">Type</label>
      <select name="type_id" id="type" class="form-control mt-1" onchange="activateDate2()">
        <% List<Type> types = (List<Type>) request.getAttribute("types");
          for (Type type: types) { %>
        <option value="<%=type.getId()%>"
        <% Post post = (Post) request.getAttribute("command");
          if (post.getType() != null && type.getId().equals(post.getType().getId())) { %>
                selected
        <% } %>
        ><%=type.getName()%></option>
        <% } %>
      </select>
    </div>

    <div class="mb-3">
      <label for="title">Title</label>
      <form:input path="title" class="form-control mt-1" id="title" placeholder="Title" value="${command.title}"/>
    </div>

    <div class="mb-3">
      <label for="description">Description</label>
      <form:textarea path="description" rows="8" class="form-control mt-1" id="description" placeholder="Description" value="${command.description}"/>
    </div>

    <div class="row">
      <div class="form-group col-md-6 mb-3">
        <label for="date1">Date 1</label>
        <form:input path="date1" type="datetime-local" class="form-control mt-1" id="date1" placeholder="Date1" value="${command.date1}"/>
      </div>

      <div class="form-group col-md-6 mb-3">
        <label for="date2">Date 2</label>
        <form:input path="date2" type="datetime-local" class="form-control mt-1" id="date2" placeholder="Date2" value="${command.date2}" disabled="${command.isArticle()}"/>
      </div>
    </div>

    <div class="mb-3">
      <label for="location">Location</label>
      <form:input path="location" class="form-control mt-1" id="location" placeholder="Location" value="${command.location}"/>
    </div>

    <div class="mb-3">
      <label for="image">Image</label>
      <form:input path="imageFile" type="file" id="image" class="form-control mt-1"/>
    </div>

    <c:if test='${event == "update" && side == "admin"}'>
    <div class="form-group col-md-6 mb-3">
      <label for="pub_date">Publication Date</label>
      <form:input path="pub_date" type="datetime-local" class="form-control mt-1" id="pub_date" placeholder="PubDate" value="${command.pub_date}"/>
    </div>

      <div class="mb-3">
        <label for="status">Status</label>
        <select class="form-control mt-1" name="status" id="status">
            <option value="1"
              <c:if test='${command.status == 1 }'>
                selected
              </c:if>
    >Active</option>
            <option value="0"
              <c:if test='${command.status == 0 }'>
                selected
              </c:if>
            >Inactive</option>
        </select>
      </div>

      <div class="mb-3">
        <label for="priority">Priority</label>
        <select class="form-control mt-1" name="is_priority" id="priority">
          <option value="1"
          <c:if test='${command.is_priority == 1 }'>
            selected
          </c:if>
          >Priority</option>
          <option value="0"
            <c:if test='${command.is_priority == 0 }'>
              selected
            </c:if>
          >Not Priority</option>
        </select>
      </div>

    </c:if>



      <div class="row">
        <div class="col text-end mt-2">
          <button type="submit" class="btn btn-success btn-lg px-3">Send</button>
        </div>
      </div>
    </form:form>
    <p>${error}</p>
  </div>
</div>


<jsp:include page="footer_${side}.jsp" />
</html>
