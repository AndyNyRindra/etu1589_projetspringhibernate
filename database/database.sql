--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: admin; Type: TABLE; Schema: public; Owner: article
--

CREATE TABLE public.admin (
    id integer NOT NULL,
    name character varying NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL
);


ALTER TABLE public.admin OWNER TO article;

--
-- Name: admin_id_seq; Type: SEQUENCE; Schema: public; Owner: article
--

CREATE SEQUENCE public.admin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.admin_id_seq OWNER TO article;

--
-- Name: admin_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: article
--

ALTER SEQUENCE public.admin_id_seq OWNED BY public.admin.id;


--
-- Name: post; Type: TABLE; Schema: public; Owner: article
--

CREATE TABLE public.post (
    id integer NOT NULL,
    title character varying NOT NULL,
    description text NOT NULL,
    date1 date NOT NULL,
    date2 date,
    type_id integer NOT NULL,
    location_id integer,
    image text
);


ALTER TABLE public.post OWNER TO article;

--
-- Name: article_id_seq; Type: SEQUENCE; Schema: public; Owner: article
--

CREATE SEQUENCE public.article_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.article_id_seq OWNER TO article;

--
-- Name: article_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: article
--

ALTER SEQUENCE public.article_id_seq OWNED BY public.post.id;


--
-- Name: location; Type: TABLE; Schema: public; Owner: article
--

CREATE TABLE public.location (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.location OWNER TO article;

--
-- Name: location_id_seq; Type: SEQUENCE; Schema: public; Owner: article
--

CREATE SEQUENCE public.location_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.location_id_seq OWNER TO article;

--
-- Name: location_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: article
--

ALTER SEQUENCE public.location_id_seq OWNED BY public.location.id;


--
-- Name: type; Type: TABLE; Schema: public; Owner: article
--

CREATE TABLE public.type (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.type OWNER TO article;

--
-- Name: type_id_seq; Type: SEQUENCE; Schema: public; Owner: article
--

CREATE SEQUENCE public.type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_id_seq OWNER TO article;

--
-- Name: type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: article
--

ALTER SEQUENCE public.type_id_seq OWNED BY public.type.id;


--
-- Name: admin id; Type: DEFAULT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.admin ALTER COLUMN id SET DEFAULT nextval('public.admin_id_seq'::regclass);


--
-- Name: location id; Type: DEFAULT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.location ALTER COLUMN id SET DEFAULT nextval('public.location_id_seq'::regclass);


--
-- Name: post id; Type: DEFAULT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.post ALTER COLUMN id SET DEFAULT nextval('public.article_id_seq'::regclass);


--
-- Name: type id; Type: DEFAULT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.type ALTER COLUMN id SET DEFAULT nextval('public.type_id_seq'::regclass);


--
-- Data for Name: admin; Type: TABLE DATA; Schema: public; Owner: article
--

COPY public.admin (id, name, email, password) FROM stdin;
\.


--
-- Data for Name: location; Type: TABLE DATA; Schema: public; Owner: article
--

COPY public.location (id, name) FROM stdin;
\.


--
-- Data for Name: post; Type: TABLE DATA; Schema: public; Owner: article
--

COPY public.post (id, title, description, date1, date2, type_id, location_id, image) FROM stdin;
\.


--
-- Data for Name: type; Type: TABLE DATA; Schema: public; Owner: article
--

COPY public.type (id, name) FROM stdin;
\.


--
-- Name: admin_id_seq; Type: SEQUENCE SET; Schema: public; Owner: article
--

SELECT pg_catalog.setval('public.admin_id_seq', 1, false);


--
-- Name: article_id_seq; Type: SEQUENCE SET; Schema: public; Owner: article
--

SELECT pg_catalog.setval('public.article_id_seq', 1, false);


--
-- Name: location_id_seq; Type: SEQUENCE SET; Schema: public; Owner: article
--

SELECT pg_catalog.setval('public.location_id_seq', 1, false);


--
-- Name: type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: article
--

SELECT pg_catalog.setval('public.type_id_seq', 1, false);


--
-- Name: post pk_article; Type: CONSTRAINT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT pk_article PRIMARY KEY (id);


--
-- Name: admin pk_tbl; Type: CONSTRAINT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.admin
    ADD CONSTRAINT pk_tbl PRIMARY KEY (id);


--
-- Name: type pk_tbl_0; Type: CONSTRAINT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.type
    ADD CONSTRAINT pk_tbl_0 PRIMARY KEY (id);


--
-- Name: location pk_tbl_1; Type: CONSTRAINT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.location
    ADD CONSTRAINT pk_tbl_1 PRIMARY KEY (id);


--
-- Name: post fk_post_location; Type: FK CONSTRAINT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT fk_post_location FOREIGN KEY (location_id) REFERENCES public.location(id);


--
-- Name: post fk_post_type; Type: FK CONSTRAINT; Schema: public; Owner: article
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT fk_post_type FOREIGN KEY (type_id) REFERENCES public.type(id);


--
-- PostgreSQL database dump complete
--

